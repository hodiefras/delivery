﻿namespace DeliveryManager.Core
{
    public class User
    {
        public User(int userId)
        {
            this.Id = userId;
        }

        public int Id { get; private set; }
    }
}
