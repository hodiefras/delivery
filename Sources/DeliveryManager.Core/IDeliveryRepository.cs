﻿using System.Collections.Generic;

namespace DeliveryManager.Core
{
    public interface IDeliveryRepository
    {
        void Add(Delivery delivery);

        Delivery Get(int deliveryId);

        IEnumerable<Delivery> Get(DeliveryStatus? status);

        void Store(Delivery delivery);
    }
}
