﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DeliveryManager.Core
{
    public class DeliveryService : IDeliveryService
    {
        private readonly IDeliveryRepository deliveryRepository;
        private readonly TimeSpan deliveryTtl;

        public DeliveryService(IDeliveryRepository deliveryRepository, TimeSpan deliveryTtl)
        {
            this.deliveryRepository = deliveryRepository;
            this.deliveryTtl = deliveryTtl;
        }

        public Delivery CreateDelivery(string title)
        {
            var delivery = new Delivery(title, DateTime.UtcNow + this.deliveryTtl);
            this.deliveryRepository.Add(delivery);

            return delivery;
        }

        public Delivery GetDelivery(int deliveryId)
        {
            return this.deliveryRepository.Get(deliveryId);
        }

        public IEnumerable<Delivery> GetDeliveries(DeliveryStatus? status)
        {
            return this.deliveryRepository.Get(status);
        }

        public Delivery AllocateDelivery(int deliveryId, int userId)
        {
            Delivery delivery = this.deliveryRepository.Get(deliveryId);
            if (delivery == null)
            {
                return null;
            }

            delivery.Allocate(userId);
            this.deliveryRepository.Store(delivery);

            return delivery;
        }

        public void CollectExpiredDeliveries()
        {
            foreach (Delivery d in this.deliveryRepository.Get(null).Where(d => d.IsExpired))
            {
                if (d.Exceed())
                {
                    this.deliveryRepository.Store(d);
                }
            }
        }
    }
}
