﻿using System;

namespace DeliveryManager.Core
{
    public class Delivery
    {
        public Delivery(string title, DateTime expirationTime)
        {
            this.Id = 0;
            this.Status = DeliveryStatus.Available;
            this.Title = title;
            this.CreationTime = DateTime.UtcNow;
            this.ExpirationTime = expirationTime;
            this.ModificationTime = this.CreationTime;
        }

        public int Id { get; private set; }

        public DeliveryStatus Status { get; private set; }

        public string Title { get; private set; }

        public User User { get; private set; }

        public DateTime CreationTime { get; private set; }

        public DateTime ExpirationTime { get; private set; }

        public DateTime ModificationTime { get; private set; }

        public bool IsExpired
        {
            get
            {
                return ExpirationTime < DateTime.UtcNow;
            }
        }

        public void Allocate(int userId)
        {
            if (this.Status != DeliveryStatus.Available)
            {
                throw new InvalidOperationException();
            }

            this.User = new Core.User(userId);
            this.Status = DeliveryStatus.Taken;
            this.ModificationTime = DateTime.UtcNow;
        }

        public bool Exceed()
        {
            if (!this.IsExpired || this.Status == DeliveryStatus.Expired)
            {
                return false;
            }

            this.Status = DeliveryStatus.Expired;
            this.ModificationTime = DateTime.UtcNow;

            return true;
        }

    }
}
