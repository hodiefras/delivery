﻿using System.Collections.Generic;

namespace DeliveryManager.Core
{
    public interface IDeliveryService
    {
        Delivery CreateDelivery(string title);
        Delivery GetDelivery(int deliveryId);
        IEnumerable<Delivery> GetDeliveries(DeliveryStatus? status);
        Delivery AllocateDelivery(int deliveryId, int userId);
        void CollectExpiredDeliveries();
    }
}
