﻿namespace DeliveryManager.Core
{
    public enum DeliveryStatus
    {
        Available,
        Taken,
        Expired
    }
}
