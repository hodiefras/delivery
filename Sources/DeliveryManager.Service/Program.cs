﻿using System.Configuration;
using Topshelf;
using Topshelf.Common.Logging;

namespace DeliveryManager.Service
{
    class Program
    {
        static void Main(string[] args)
        {
            HostFactory.Run(cfg => 
            {
                cfg.UseCommonLogging();
                cfg.Service<ServiceInstance>(s => 
                {
                    s.ConstructUsing(() => new ServiceInstance(ConfigurationManager.AppSettings["baseAddress"]));
                    s.WhenStarted(si => si.Start());
                    s.WhenStopped(si => si.Stop());
                });
            });
        }
    }
}
