﻿using Autofac;
using Autofac.Integration.WebApi;
using Common.Logging;
using DeliveryManager.Core;
using DeliveryManager.Data;
using Owin;
using System;
using System.Configuration;
using System.Reflection;
using System.Web.Http;

namespace DeliveryManager.Service
{
    public class Startup
    {
        private static readonly ILog Log = LogManager.GetLogger<Startup>();

        public void ConfigureContainer(IAppBuilder appBuilder, HttpConfiguration config)
        {
            Log.Info("Инициализация контейнера.");
            var containerBuilder = new ContainerBuilder();

            containerBuilder
                .RegisterType<DeliveryService>()
                .As<IDeliveryService>()
                .SingleInstance()
                .WithParameter(
                    "deliveryTtl",
                    TimeSpan.Parse(ConfigurationManager.AppSettings["deliveryTtl"]));

            containerBuilder
                .RegisterType<DeliveryRepository>()
                .As<IDeliveryRepository>()
                .SingleInstance();

            containerBuilder.
                RegisterApiControllers(Assembly.GetExecutingAssembly());

            IContainer container = containerBuilder.Build();
            appBuilder.UseAutofacMiddleware(container);

            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);
        }

        public void Configuration(IAppBuilder appBuilder)
        {
            Log.Info("Инициализация приложения.");
            var config = new HttpConfiguration();

            ConfigureContainer(appBuilder, config);

            config.MapHttpAttributeRoutes();

            appBuilder.UseWebApi(config);
        }
    }
}
