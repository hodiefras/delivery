﻿using Common.Logging;
using Microsoft.Owin.Hosting;
using System;

namespace DeliveryManager.Service
{
    class ServiceInstance
    {
        private static readonly ILog Log = LogManager.GetLogger<ServiceInstance>();

        private IDisposable webApp;
        private readonly string baseAddress;

        public ServiceInstance(string baseAddress)
        {
            this.baseAddress = baseAddress;
        }

        public void Start()
        {
            Log.Info("Запуск сервиса.");
            this.webApp = WebApp.Start<Startup>(this.baseAddress);
        }

        public void Stop()
        {
            Log.Info("Остановка сервиса.");
            this.webApp.Dispose();
        }
    }
}
