﻿using DeliveryManager.Core;
using System.Web.Http;
using System;
using System.Linq;
using Common.Logging;

namespace DeliveryManager.Service.Deliveries
{
    [RoutePrefix("deliveries")]
    public class DeliveryController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger<DeliveryController>();

        private readonly IDeliveryService deliveryService;

        public DeliveryController(IDeliveryService deliveryService)
        {
            this.deliveryService = deliveryService;
        }

        [Route("", Name = "getDeliveries")]
        [HttpGet]
        public IHttpActionResult FindAllDeliveries([FromUri]string status = null)
        {
            Log.Debug("Получение всех доставок.");

            DeliveryStatus? s = 
                status == null ? 
                null : 
                (DeliveryStatus?)Enum.Parse(typeof(DeliveryStatus), status, true);

            return Ok(this.deliveryService.GetDeliveries(s).Select(d => CreateDelivery(d)));
        }

        [Route("{deliveryId}", Name = "getDeliveryById")]
        [HttpGet]
        public IHttpActionResult FindDelivery(int deliveryId)
        {
            Log.Debug("Получение доставки.");

            var delivery = this.deliveryService.GetDelivery(deliveryId);
            if (delivery == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(CreateDelivery(delivery));
            }
        }

        [Route("")]
        [HttpPost]
        public IHttpActionResult CreateDelivery([FromBody]NewDelivery delivery)
        {
            Log.Debug("Создание доставки.");

            Core.Delivery createdDelivery = this.deliveryService.CreateDelivery(delivery.Title);
            var link = Url.Link("getDeliveryById", new { deliveryId = createdDelivery.Id });
            return new CreatedDeliveryActionResult(this.Request, link);
        }

        [Route("{deliveryId}/user")]
        [HttpPost]
        public IHttpActionResult AllocateDelivery([FromUri]int deliveryId, [FromBody]DeliveryUser user)
        {
            Log.Debug("Резервирование доставки.");

            try
            {
                Core.Delivery updatedDelivery = this.deliveryService.AllocateDelivery(deliveryId, user.Id);
                if (updatedDelivery == null)
                {
                    return NotFound();
                }
                else
                {
                    string link = Url.Link("getDeliveryById", new { deliveryId = updatedDelivery.Id });
                    return new UpdatedDeliveryActionResult(this.Request, link);
                }
            }
            catch (InvalidOperationException)
            {
                return new InvalidDeliveryActionResult(this.Request);
            }
        }

        [Route("expired")]
        [HttpPost]
        public IHttpActionResult CollectExpiredDeliveries()
        {
            Log.Debug("Сбор просроченных доставок.");

            this.deliveryService.CollectExpiredDeliveries();

            string link = Url.Link("getDeliveries", new { });
            return new ExpiredDeliveryActionResult(this.Request, link);
        }

        private static Delivery CreateDelivery(Core.Delivery delivery)
        {
            return new Delivery
            {
                Id = delivery.Id,
                Title = delivery.Title,
                Status = Enum.GetName(typeof(DeliveryStatus), delivery.Status),
                CreationTime = delivery.CreationTime,
                ModificationTime = delivery.ModificationTime,
                UserId = delivery.User?.Id
            };
        }
    }
}
