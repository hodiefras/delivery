﻿using System;

namespace DeliveryManager.Service.Deliveries
{
    public class Delivery
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Status { get; set; }
        public int? UserId { get; set; }
        public DateTime CreationTime { get; set; }
        public DateTime ModificationTime { get; set; }
    }
}
