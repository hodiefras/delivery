﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;

namespace DeliveryManager.Service.Deliveries
{
    class CreatedDeliveryActionResult : IHttpActionResult
    {
        private readonly HttpRequestMessage request;
        private readonly string location;

        public CreatedDeliveryActionResult(HttpRequestMessage request, string location)
        {
            this.request = request;
            this.location = location;
        }

        public Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
        {
            var response = this.request.CreateResponse(HttpStatusCode.Created);
            response.Headers.Location = new Uri(this.location);
            return Task.FromResult(response);
        }
    }
}
