﻿using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;

namespace DeliveryManager.Service.Deliveries
{
    public class InvalidDeliveryActionResult : IHttpActionResult
    {
        private readonly HttpRequestMessage request;

        public InvalidDeliveryActionResult(HttpRequestMessage request)
        {
            this.request = request;
        }

        public Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
        {
            var response = this.request.CreateResponse((HttpStatusCode)422);
            return Task.FromResult(response);
        }
    }
}
