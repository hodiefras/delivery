﻿using DeliveryManager.Core;
using System.Collections.Generic;
using System.Linq;

namespace DeliveryManager.Data
{
    public class DeliveryRepository : IDeliveryRepository
    {
        private IList<Delivery> deliveries = new List<Delivery>();

        public void Add(Delivery delivery)
        {
            deliveries.Add(delivery);
        }

        public Delivery Get(int deliveryId)
        {
            return this.deliveries.FirstOrDefault(d => d.Id == deliveryId);
        }

        public IEnumerable<Delivery> Get(DeliveryStatus? status)
        {
            return this.deliveries.Where(d => status == null || d.Status == status);
        }

        public void Store(Delivery delivery)
        {
            return;
        }
    }
}
